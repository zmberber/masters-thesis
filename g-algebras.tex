\chapter{G-Algebras}

% We go by \cite{lev05}.
In our discussion of noncommutative Gr\"obner bases, our main hindrance was that our $k$-algebra was not Noetherian in general, meaning that ideals are not necessarily always finitely generated, and even if they are, we cannot guarantee a finite Gr\"obner basis.
There are examples of noncommutative $k$-algebras that are Noetherian, for example universal enveloping algebras $\Ue (\lie g)$ of finite dimensional Lie algebras $\lie g$, where we have the following nice properties.
\begin{itemize}
\item The Poincar\'e-Birkhoff-Witt (PBW) theorem:  For a basis $\set{x_1,\dotsc,x_n}$ of $\lie g$, the set $ \set{ x_1^{\lambda_1}  \dotso x_n^{\lambda_n} \given \lambda_i \in \mathbb N_0}$ is a $k$-basis of $\Ue (\lie g)$.
\item It is \enquote{close to commutative}:
  For $i < j$ we have $x_j \mult x_i = x_i \mult x_j - \commu{x_i,x_j}$ due to $\commu{x_i,x_j} = x_i \mult x_j - x_j \mult x_i$ as per definition.
  Furthermore $\commu{x_i,x_j}$ has degree $1$ and $x_j  x_i$ and $x_i x_j$ have degree $2$ in $\nckxn$.
\item $\Ue(\lie g)$ is left and right Noetherian.
\end{itemize}
We can define a notion of algebras that have these properties and generalize universal enveloping algebras.
These algebras are called G-algebras, and it is possible to develop Gr\"obner basis theory in such algebras.
This was first comprehensively introduced in \cite{lev05}, and these concepts were also implemented in \Singular.
We will briefly go over the main results and check some examples.

\section{Gr\"obner bases and the non-degeneracy conditions}

\begin{definition}[G-algebra, non-degeneracy conditions]\label{galgebra}
  Consider $A= \nckxn$ and let $d_{i,j} \in A$ and $c_{i,j} \in \units{k}$ for $0 \leq i < j \leq n$.
  We now define the polynomials
  \begin{equation*}
    f_{j,i} = x_j x_i - ( c_{i,j} \mult x_i x_j + d_{i,j} )
    \mathcomma
  \end{equation*}
  and $F$ as the set of all $f_{j,i}$.
  We furthermore define the \emph{non-degeneracy conditions}
  \begin{alignat*}{4}
    \ndc_{i,j,k} &= &&&
    c_{i,k}c_{j,k} \mult d_{i,j} x_k &&\, - \, & x_k d_{i,j}  \\
    &&&+ \,& c_{j,k} \mult x_j d_{i,k} &&\, - \,& c_{i,j} \mult d_{i,k} x_j  \\
    &&&+ \,& d_{j,k} x_i &&\, - \,& c_{i,j} c_{i,k} \mult x_i d_{j,k}
  \end{alignat*}
  for $0 \leq i < j < k \leq n$.

  Let furthermore $\order$ be an admissible order on $\nckxn$ such that
  \begin{equation*}
    \LT(f_{j,i}) = x_j x_i \text{ and } \LT (d_{i,j}) \orderstrict x_i x_j \text{ for all } 0 \leq i < j \leq n  \mathperiod
  \end{equation*}
  If then $\normal(\ndc_{i,j,k};F) = 0$ for all $0 \leq i < j < k \leq n$, then we call $R = A / \ideal{F}$ a \emph{G-algebra}.
\end{definition}

\begin{remark}\label{commd}
  Studying $\modulo{\nckxn}{\ideal F}$, we can without loss of generality assume that the $d_{i,j} \in \spann_k\set{x_1^{i_1} \dotso x_n^{i_n} \given i_1,\dotsc,i_n \in \N_0}$:
  If $x_kx_l$ occurs in $\supp(d_{i,j})$ such that $k > l$, we can then substitute it by $c_{j,k} x_lx_k + d_{l,k}$.
  Doing this over and over, this process must terminate, as our admissible order is a well order.
\end{remark}

Let $\lie g$ be a finite dimensional Lie algebra.
If $\set{x_1,\dotsc,x_n} \subseteq \lie g$ is a $k$-basis of $\lie g$, then the universal enveloping algebra $\Ue(\lie g)$ of $\lie g$ is given by
\begin{equation*}
  \Ue(\lie g) = \modulo{\nckxn}{\ideal{\commu{x_i,x_j} - (x_ix_j - x_jx_i)\given i,j \in \numbers n}} \mathperiod
\end{equation*}
We then have
\begin{align*}
  \commu{x_i,x_j} &= x_ix_j - x_jx_i \mathcomma \text{and therefore}  \\
  x_jx_i &= x_ix_j - \commu{x_i,x_j}
\end{align*}
in $\Ue(\lie g)$ for all $i,j \in \numbers n$, in particular for all $1 \leq i < j \leq n$.
Looking at \Cref{galgebra}, we can define $f_{j,i} \defeq x_jx_i - (c_{i,j}x_ix_j + d_{i,j})$ for $1 \leq i \leq j \leq n$ with
\begin{align*}
  c_{i,j} &= 1  \\
  d_{i,j} &= -\commu{x_i,x_j}  \mathcomma
\end{align*}
and we notice that $\Ue(\lie g) \isom \modulo{\nckxn}{\ideal F}$, where $F$ is the set of all $f_{j,i}$.
The non-degeneracy conditions are fulfilled as a result of the Jacobian identity, so in some sense the non-degeneracy conditions are a generalization of the Jacobian identity.
It is then not surprising that PBW basis theorem generalizes to G-algebras, which is the following theorem.


% \begin{theorem}
%   In the setting of \Cref{galgebra}, $F$ is a Gr\"obner basis for $\ideal F$ if and only if $\normal(\ndc_{i,j,k};F) = 0$ for all $0 \leq i < j < k \leq n$.
% \end{theorem}

% \begin{theorem}
%   A G-algebra with the above data has PBW-basis $\set{x_1^{\lambda_1} \mult \dotsm \mult x_n^{\lambda_n} \given \lambda_i \in \N_0}$.
%   In fact, in the setup above, there exists a PBW-basis if and only if the non-degeneracy conditions hold.
% \end{theorem}

\begin{theorem}
  In the setting of \Cref{galgebra}, the following are equivalent.
  \begin{enumerate}
  \item The set $F$ is a Gr\"obner basis for $\ideal F$.
  \item For all $0 \leq i < j < k \leq n$ we have $\normal(\ndc_{i,j,k};F) = 0$ (for any normal remainder).
  \item The $k$-algebra $\modulo{\nckxn}{\ideal F}$ has a PBW basis 
  \end{enumerate}
\end{theorem}

\begin{proof}
  See \cite[Theorem~2.3]{lev05}.
\end{proof}

\begin{remark}
  Once we fix an order for the generating variables $x_1 \orderstrict \dotsb \orderstrict x_n$, it doesn't matter what admissible order we choose on $\nckxn$ that fulfills the requirements in \Cref{galgebra}, the resulting PBW basis will always be the same.
\end{remark}

% \begin{example}[universal enveloping algebras]
%   Let $\lie g$ be a Lie algebra, and let $T(\lie g)$ be the tensor algebra over the underlying vector field of $\lie g$.
%   The universal enveloping algebra $\Ue(\lie g)$ then is isomorphic to $\modulo{T(\lie g)}{I}$, where $I$ is the ideal generated by all elements of the form
%   \begin{equation*}
%     \commu{g_1,g_2} - (g_1 g_2 - g_2 g_1)
%   \end{equation*}
%   for $g_1,g_2 \in \lie g$.
%   The PBW theorem now says that for any basis $\dottedc{x}{1}{n}$, we have a PBW-basis $\set{x_1^{\lambda_1} \mult \dotsm \mult x_n^{\lambda_n} \given \lambda_i \in \N_0}$ of $\Ue(\lie g)$.

%   We then also have that (a very slight abuse of notation) that $T(\lie g)$ is isomorphic to $T_n = \nckxn$, and for the ideal in here that corresponds to $I$ in $T(\lie g)$, by bilinearity of the Lie bracket we can choose in $\nckxn$ the finite generators
%   \begin{equation*}
%     f_{j,i} = x_j x_i - (x_i x_j + \commu{x_j,x_i})
%     \mathcomma
%   \end{equation*}
%   for $0 \leq i < j \leq n$, the set of which we shall call $F$, and we then have $\Ue(\lie g) \isom \modulo{T_n}{\ideal{F}}$.

%   We now have the exact same setup as in the definition of G-algebras, and we can choose any admissible order where all polynomials of degree $1$ are smaller than all the polynomials of degree $2$, and such that $x_i x_j \orderstrict x_j x_i$ for all $0 \leq i < j \leq n$, for example the left degree lexicographic order \verb|Dp|.
%   % The right length lexicographic order with $x_n \orderstrict \dotsb \orderstrict x_1$, will do (or alternatively the left lexicographic order with $x_1 \orderstrict \dotsb \orderstrict x_n$).
% \end{example}

\section{Checking the non-degeneracy conditions in \Singular}

Let us now look at $\Ue(\lie{sl}_2)$, which we want to define and work with in \Singular.

\begin{example}
  The universal enveloping algebra $\Ue(\lie{sl}_2)$ of $\lie{sl}_2$ is defined by the relations
  \begin{align*}
    f_{2,1} &= fe - (ef - h)  \\
    f_{3,1} &= he - (eh + 2e)  \\
    f_{3,2} &= hf - (fh - 2f)
  \end{align*}
  in $k \freemonoid{e,f,h}$, where we named the polynomials as in \Cref{galgebra}.
  We also know that this is a Gr\"obner basis for the ideal it generates, as the non-degeneracy conditions hold.

  We now have the following values for our $c_{i,j}$ and $d_{i,j}$.
  \begin{align*}
    c_{1,2} = c_{1,3} = c_{2,3} &= 1 \\
    d_{1,2} &= -h \\
    d_{1,3} &= 2e  \\
    d_{2,3} &= -2f
  \end{align*}
  As mentioned in \Cref{commd}, the $d_{i,j}$ can be chosen to be in $\set{x_1^{\lambda_1} \mult \dotsm \mult x_n^{\lambda_n} \given \lambda_i \in \N_0}$.
  Indeed, internally, \Singular\ requires the elements $d_{i,j}$ to be in $\kxn$.
  For universal enveloping algebras, and therefore in our example, this is not a problem as the $d_{i,j}$ are of degree $1$.

  To define a G-algebra, we can input these into matrices \verb|C| and \verb|D|.
\begin{verbatim}
> ring r = 0,(e,f,h),Dp;
> setring r;
> matrix D[3][3];  // initialize a 3x3 matrix
> D[1,2]=-h; D[1,3]=2e; D[2,3]=-2f;  // input values
\end{verbatim}

  We can now define our G-algebra with the function \verb|nc_algebra|, where we must input our matrices \verb|C| and \verb|D|.
  The function \verb|nc_algebra| will only consider the strict upper entries, and if we input a value instead of a matrix, \Singular\ will use that value for every entry it expects.
  This means that to define our G-algebra and work with it, we can now input the following.
\begin{verbatim}
> LIB "freegb.lib";
> def A = nc_algebra(1,D);
> setring A;
> A;
\end{verbatim}
\begin{verbatim}
// coefficients: QQ
// number of vars : 3
//        block   1 : ordering Dp
//                  : names    e f h
//        block   2 : ordering C
// noncommutative relations:
//    fe=ef-h
//    he=eh+2e
//    hf=fh-2f
\end{verbatim}

  There is one tricky detail with \Singular\ that should be pointed out.
  So far, we have only typed in data of elements in $k \cmonoid{e,f,h}$, as is expected by \Singular.
  When we work in the G-algebra \verb|A|, we must write \verb|*| between variables.
  For instance, if we have the element $hfe$, \verb|hfe| will be treated as residing in $k \cmonoid{e,f,h}$, so we shall type in \verb|h*f*e|.
  The output will always be displayed in $k \cmonoid{e,f,h}$.
\begin{verbatim}
> hfe;
efh
> h*f*e;
efh-h2
\end{verbatim}
  What the \verb|*| operation does is replace $x_j x_i$ by $x_ix_j + d_{i,j}$ if $i < j$.
  With the same argument as in \Cref{commd}, as $d_{i,j}$ is strictly smaller than $x_jx_i$, this process will stop after finitely many steps, since we have an admissible order.
  After this process has ended, the final output will be presented in terms of $\set{x_1^{\lambda_1} \mult \dotsm \mult x_n^{\lambda_n} \given \lambda_i \in \N_0}$.
  % Internally what happens is that every term (a word that consists of only letters and numbers and is not a subword of another such word) is treated as being in $k \cmonoid{e,f,h}$, and \verb|*| being a multiplication in $k \cmonoid{e,f,h}$ that morally does the following.
  % If we have an expression of the form $u \ast w$ for two terms $u$ and $w$, we get $u \ast w = \Phi(\overline{\Delta(u)} \mult \overline{\Delta(w)})$, where $\Delta \colon k \cmonoid{e,f,h} \to k \freemonoid{e,f,h}$ is as in \Cref{commorder} and $\Phi \colon k \Ue{\slt} \to k \cmonoid{e,f,h}$ is the linear extension of $\prod_{i=1}^r x_i \mapsto \prod_{i=1}^r x_i$ for an element $\prod_{i=1}^r x_i$ on the left hand side a PBW-basis element (which is equivalent to $x_{i+1} \orderstrict x_i$ for all $i$).
  % We have associativity of this.
  Via the PBW theorem this is a basis, meaning that this operation must be associative by uniqueness.
\begin{verbatim}
> f*f*e*h;
ef2h-2fh2+2fh
> e*f*e*h;
e2fh-eh2
> f*f*e*h*e*f*e*h;
e3f3h2+2e3f3h-7e2f2h3-8e2f2h2+10efh4+12e2f2h+14efh3-2h5-12efh2-6h4-4h3
> (ef2h-2fh2+2fh)*(e2fh-eh2);
e3f3h2+2e3f3h-7e2f2h3-8e2f2h2+10efh4+12e2f2h+14efh3-2h5-12efh2-6h4-4h3
> f*f*e*(eh+2e)*f*e*h;
e3f3h2+2e3f3h-7e2f2h3-8e2f2h2+10efh4+12e2f2h+14efh3-2h5-12efh2-6h4-4h3
\end{verbatim}
\end{example}

Let us see another example, a Weyl algebra.

\begin{example}[Weyl Algebra]\label{weyl}
  We shall present the example of the Weyl algebra.
  The Weyl algebra is important in physics, especially in quantum mechanics, where it describes the space of linear operators on the Hilbert space of $L^2$ generated by the position operators and momentum operators.
  We fix $n \in \N_{>0}$ and for each $i \in \numbers n$ we have generators $x_i$ and $\partial_i$, subject to the following relations:
  \begin{itemize}
  \item $\partial_i x_i = x_i \partial_i + 1$, the chain rule,
  \item $\partial_i \partial_j = \partial_j \partial_i$, Schwarz's theorem, and
  \item $x_i x_j = x_j x_i$, commutativity.
  \end{itemize}
  If we order our elements $x_1 \orderstrict \dotsb \orderstrict x_n \orderstrict \partial_1 \orderstrict \dotsb \orderstrict \partial_n$ and then choose the left degree lexicographic order on $k \freemonoid{x_1,\dotsc,x_n,\partial_{x_1},\dotsc,\partial_{x_n}}$, defining the $f_{j,i}$ according to those equations fulfills our requirements.

  Let us implement this in \Singular\ with $n=3$, and check if the non-degeneracy conditions hold.
  We have the values $c_{i,j} = 1$ for all $i,j$, $d_{1,4} = d_{2,5} = d_{3,6} = 1$, and $d_{i,j} = 0$ for all other $i,j$.
  We can now input our data into \Singular.
\begin{verbatim}
> LIB "freegb.lib";
> ring r = 0,(x,y,z,dx,dy,dz),dp;
> setring r;
> matrix D[6][6];
> D[1,4] = 1; D[2,5] = 1; D[3,6] = 1;
> def A = nc_algebra(1,D);
> setring A;
> A;
\end{verbatim}
\begin{verbatim}
// coefficients: QQ
// number of vars : 6
//        block   1 : ordering Dp
//                  : names    x y z dx dy dz
//        block   2 : ordering C
// noncommutative relations:
//    dxx=x*dx+1
//    dyy=y*dy+1
//    dzz=z*dz+1
\end{verbatim}
  The function \verb|nc_algebra| expects two matrices.
  The first argument is the matrix $C$ obtained from the $c_{i,j}$ and the second argument is the matrix obtained from the $d_{i,j}$, and the function only regards values for $i<j$, 
  Note that when we initialize the matrix \verb|D|, all values are $0$.
  \Singular\  has the function \verb|ndcond()| which checks if the non-degeneracy conditions hold.
\begin{verbatim}
> printlevel = 1;   // verbose output
> ndcond();
\end{verbatim}
\begin{verbatim}
Processing degree : 1
1 . 2 . 3 .
1 . 2 . 4 .
1 . 2 . 5 .
1 . 2 . 6 .
1 . 3 . 4 .
1 . 3 . 5 .
1 . 3 . 6 .
1 . 4 . 5 .
1 . 4 . 6 .
1 . 5 . 6 .
2 . 3 . 4 .
2 . 3 . 5 .
2 . 3 . 6 .
2 . 4 . 5 .
2 . 4 . 6 .
2 . 5 . 6 .
3 . 4 . 5 .
3 . 4 . 6 .
3 . 5 . 6 .
4 . 5 . 6 .
done
_[1]=0
\end{verbatim}
  So we indeed have a G-algebra.
  The output indicates which non-degeneracy condition $\ndc_{i,j,k}$ it is checking.
  
  If we change our relations, we can see an example that isn't a G-algebra, and \Singular\  will inform which non-degeneracy condition $\ndc_{i,j,k}$ is not fulfilled.
\begin{verbatim}
ring r = 0,(x,y,z,dx,dy,dz),Dp;
setring r;
matrix D[6][6];
D[1,4] = dz*z+x; D[2,5] = 1; D[3,6] = 1;
def A = nc_algebra(1,D);
setring A;
A;
\end{verbatim}
\begin{verbatim}
// coefficients: QQ
// number of vars : 6
//        block   1 : ordering Dp
//                  : names    x y z dx dy dz
//        block   2 : ordering C
// noncommutative relations:
//    dxx=x*dx+z*dz+x
//    dyy=y*dy+1
//    dzz=z*dz+1
\end{verbatim}
\begin{verbatim}
> ndcond();
\end{verbatim}
\begin{verbatim}
Processing degree : 1
1 . 2 . 3 .
1 . 2 . 4 .
1 . 2 . 5 .
1 . 2 . 6 .
1 . 3 . 4 .
failed: -z
1 . 3 . 5 .
1 . 3 . 6 .
1 . 4 . 5 .
1 . 4 . 6 .
failed: -dz
1 . 5 . 6 .
2 . 3 . 4 .
2 . 3 . 5 .
2 . 3 . 6 .
2 . 4 . 5 .
2 . 4 . 6 .
2 . 5 . 6 .
3 . 4 . 5 .
3 . 4 . 6 .
3 . 5 . 6 .
4 . 5 . 6 .
done
_[1]=-z
_[2]=-dz
\end{verbatim}

  We also see in this example how the associativity of \verb|*| can go wrong when we don't have a PBW basis.
  Different orders in which we perform the operation as described in \Cref{commd} can lead to different results.  
% \begin{verbatim}
% > ring r = 0,(x,y,z,dx,dy,dz),Dp;
% > setring r;
% > matrix D[6][6];
% > D[1,4] = z*dz+x; D[2,5] = 1; D[3,6] = 1;
% > def A = nc_algebra(1,D);
% > setring A;
% > A;
% \end{verbatim}
% \begin{verbatim}
% // coefficients: QQ
% // number of vars : 6
% //        block   1 : ordering Dp
% //                  : names    x y z dx dy dz
% //        block   2 : ordering C
% // noncommutative relations:
% //    dxx=x*dx+z*dz+x
% //    dyy=y*dy+1
% //    dzz=z*dz+1
% \end{verbatim}
\begin{verbatim}
> (dz*dx)*x;
> dz*(dx*x);
\end{verbatim}
\begin{verbatim}
x*dx*dz+z*dz^2+x*dz
x*dx*dz+z*dz^2+x*dz+dz
\end{verbatim}
\end{example}


\section{Gr\"obner bases in G-algebras}

The PBW basis for a G-algebra is not a multiplicative basis, so we cannot apply our theory of Gr\"obner bases from before to them as is.
Still, it is indeed possible to also develop a theory of Gr\"obner bases for G-algebras, which resembles the theory o Gr\"obner bases in commutative $k$-algebras.
We will briefly discuss what is possible.
In the following, $A$ will denote a G-algebra presented as $\modulo{\nckxn}{\ideal F}$ as in \Cref{galgebra} with PBW basis $\set{x_1^{\lambda_1} \mult \dotsm \mult x_n^{\lambda_n} \given \lambda_i \in \N_0}$.
An ideal $I \subseteq A$ will refer to a left ideal in $A$, and we write $\prescript{}{A}{\ideal M}$ for the left ideal generated by $M$ in $A$.
Let furthermore $\order$ be an admissible order on $\freemonoid{x_1,\dotsc,x_n} \subseteq \kxn$.
The set $\B$ will refer to the PBW basis of $A$.

The underlying vector spaces of $A$ and $\kxn$ are the same, and $\B = \cmonoid{x_1,\dotsc,x_n}$ as sets, so $\order$ is a well order on $\B$, but we can't call quite it an admissible order, since in $A$, the set $\B \union \set 0$ is not closed under multiplication.
% To avoid confusion, from now on, the multiplication in $A$ shall be denoted by $\ast$, and the multiplication in $\kxn$ shall be denoted by $\mult$.

We will now define alternate versions of the terminology in Gr\"obner basis theory for G-algebras, as is described in  \cite[Definition~1.8]{lvy05}.

\begin{definition}
  Let $G \subseteq A$ be a subset, $I \subseteq A$ be a left ideal, and let $f,g \in G$.
  \begin{itemize}
  \item We define $\LT(f)$, $\LC(f)$, $\LM(f)$ to be just as in \Cref{leading}, where we view $f \in \kxn$ to be a commutative polynomial.
    We also define $\LT\set G$ and $\LT(F)$ by viewing $F \subseteq \kxn$.

  \item We call $G$ a left \emph{Gr\"obner basis} of $I$ if $\prescript{}{A}{\ideal G} = I$ and if $\LT(G) = \LT(I)$, in other words, by \Cref{monoideal}, for every $f \in I$ there exists $g \in G$ such that $\LT(g) \divides \LT(f)$ in $\kxn$.
    
  \item We define the \emph{S-polynomial} $S(f,g) \in A$ as in \Cref{commspol}.
  \item We call $\normal(\bullet;G) \colon A \to A$ a \emph{left normal form} with respect to $G$, if
    \begin{itemize}
    \item $\normal(y;G) = 0$ for $y=0$
    \item $\normal(y;G) \neq 0 \implies \LT(\normal(y;G)) \notin \LT(G)$, and
    \item $y - \normal(y;G) \in \prescript{}{A}{\ideal G}$ \mathperiod
    \end{itemize}
    for all $y \in A$.
  \item A representation $y = \sum_{g \in G} a_g \mult g$ for $a_g \in A$ is called a \emph{standard left representation} of $f$ with respect to $G$ if $\LT(y) \geq \LT(a_g g)$ for all $g \in G$.
  \end{itemize}

  \textit{(Compare to \cite[Definition~1.8]{lvy05}.)}
\end{definition}

There indeed always is a normal form with respect to $G$, which is presented in \cite[Algorithm~1.1,~p.51]{lvy05}.
We now present a version of Buchberger's criterion for G-algebras.

\begin{theorem}
  Let $I \subseteq A$ be a left ideal, and let $G \subseteq A$.
  For any left normal form $\normal(\bullet;G)$ with respect to $G$, the following are equivalent.
  \begin{itemize}
  \item $G$ is a left Gr\"obner basis of $I$.
  \item $\normal(f;G) = 0$ for all $f \in I$.
  \item Every $f \in I$ has a standard representation with respect to $G$.
  \item $\normal(S(f,g);G) = 0$ for all $f,g \in G$.
  \end{itemize}
\end{theorem}

\begin{proof}
  See \cite[Theorem~1.16]{lvy05}.
\end{proof}

With this, we also have a version of Buchberger's procedure.
Since G-algebras are Noetherian, just like in commutative polynomial rings, this procedure always terminates.

% \begin{proposition}
%   The following algorithm takes a set $G \subseteq A$ and terminates after finitely many steps, returning a left Gr\"obner basis $G'$ of $\prescript{}{A}{\ideal G}$.
\begin{algorithm}
  \caption{}
  \begin{algorithmic}[1]
    \Require{$G \subseteq A$ finite}
    \Ensure{A left Gr\"obner basis $G' \subseteq A$ of $\prescript{}{A}{\ideal G}$}
    
    \Let{$G'$}{$G$}
    \Let{FINISH}{False}
    \Statex

    \While{FINISH = False}
    \Let{FINISH}{True}
    \For{$g,h \in G'$}
    \If{$\normal(S(g,h);G') \neq 0$}
    \Let{$G'$}{$G' \union \set{S(g,h)}$}
    \Let{FINISH}{False}
    % \State{BREAK} %\Comment{can be left out}
    \EndIf
    \EndFor
    \EndWhile
    \State{\Return{$G'$}}
  \end{algorithmic}
\end{algorithm}
% \end{proposition}

Let us see this in \Singular, where \verb|std()| is implemented in the \enquote{plural} module for the computation of left Gr\"obner bases in G-algebras.
\pagebreak

\begin{example}
  Let us again work in the setting of $\Ue(\lie{sl}_2)$.
\begin{verbatim}
> LIB "freegb.lib";
> ring r = 0,(e,f,h),Dp;
> setring r;
> matrix D[3][3];
> D[1,2]=-h; D[1,3]=2e; D[2,3]=-2f;
> def A = nc_algebra(1,D);
> setring A;
\end{verbatim}
  To compute Gr\"obner bases, we proceed analogously to how we computed Gr\"obner bases with letterplace rings in \Singular.
\begin{verbatim}
> ideal I = f*e*e*h + 2e*f*f*e, h*f+ 2h*h*e;
> I;
\end{verbatim}
\begin{verbatim}
I[1]=2e2f2+e2fh-4efh-2eh2+4ef-2eh
I[2]=2eh2+8eh+fh+8e-2f
\end{verbatim}
\begin{verbatim}
> std(I);
\end{verbatim}
\begin{verbatim}
_[1]=e
_[2]=h2-2h
_[3]=127fh+8e-254f
\end{verbatim}
  Note that an expression like \verb|h2| refers to $h^2$.
\begin{verbatim}
> ideal J = ef + h + f, h3+f, e2f;
> J;
\end{verbatim}
\begin{verbatim}
J[1]=ef+f+h
J[2]=h3+f
J[3]=e2f
\end{verbatim}
\begin{verbatim}
> std(J);
\end{verbatim}
\begin{verbatim}
_[1]=h
_[2]=1105f-3948h
\end{verbatim}
  
\end{example}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "masterarbeit_lorke_stroppel"
%%% End:
