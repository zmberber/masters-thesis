\appendix

\chapter{Appendix}

\section{Multiplicative bases and semigroups with zero element}

\begin{definition}[semigroup with zero element]
  Let $S$ be a semigroup.
  We call an element $d \in S$ a \emph{zero element} or an \emph{absorbing element} if for all $s \in S$ we have $ds = sd = d$, and we call $S$ a \emph{semigroup with zero element}.
\end{definition}

\begin{remark}
  A zero element in a semigroup is unique.
\end{remark}

\begin{example}
  For any ring $R$, the underlying semigroup with the multiplication in $R$ as the semigroup operation is a semigroup with zero element.
  Specifically, neutral element of the underlying abelian group, $0$, is the zero element.

  In particular $\tuple{\Z,\mult}$ is a semigroup with zero element $0 \in \Z$.
\end{example}

\begin{proposition}\label{apcompat}
  If $I$ is a compatible ideal, then $\modulo{A}{I}$ has multiplicative basis \linebreak $\modline{\B} \setminus \set 0$.
\end{proposition}

\begin{proof}[Proof (sketch)]
  We clearly have that $\modline \B \setminus \set 0$ generates $\modulo A I$ as $\B$ generates $A$, and we now want to show that $\modline \B \setminus \set 0$ is linearly independet in $\modulo A I$.
  Let us outline the proof, leaving out a few categorical details.
  
  \begin{itemize}
  \item To a semigroup $S$ with zero element $d$, we can assign a (not necessarily unital) associative $k$-algebra $k(S)$, which is defined as the associative $k$-algebra with basis elements $S$, modulo the ideal $\ideal d$, so that $d$ corresponds to $0 \in kS$.
    This assignment is a functor from the category of semigroups with zero element to the category of associative unital $k$-algebras.
    The $k$-algebra $k(S)$ then has multiplicative basis $S \setminus \set 0$.
    \begin{equation*}
      k(\bullet) \colon \semz \to \kalg \mathcomma \; S \mapsto k(S)
    \end{equation*}
    This functor is left adjoint to the forgetful functor $\kalg \to \semz$.
    
  \item For $C \in \kalg$ and $J \subseteq C$ an ideal, there exists a quotient $\pi \colon C \twoheadrightarrow \modulo{C}{J}$, charectarized by following universal property.
    \textit{We have $\pi(J) = 0$, and any morphism $f \colon C \to D$ with $f(J) = 0$ factorizes uniquely through $\pi$.}
    \begin{equation*}
      \begin{tikzcd}
        C \ar[rrr, "f(J) = 0"] \ar[d, twoheadrightarrow, "\pi"'] & & & D  \\
        \modulo{C}{J} \ar[urrr, dashrightarrow, "\exists !"']
      \end{tikzcd}
    \end{equation*}
  \item For $S \in \semz$, and a collection of elements $b_l,b'_l \in S$ for $l \in L$, there exists a quotient $\tilde \pi \colon S \twoheadrightarrow \modulo{S}{ideal{b_l \relatesto b'_l \given l \in L}}$, characterized by the following universal property.
    \textit{We have $\tilde \pi(b_l) = \tilde \pi(b'_l)$ for all $l \in L$, and any morphism $g \colon S \to T$ with $g(b_l) = g(b'_l)$ for all $l \in L$ factorizes uniquely through $\tilde \pi$.}
    \begin{equation*}
      \begin{tikzcd}
        S \ar[rrr, "\forall l \in L \quantcolon g(b_l) = g(b'_l)"] \ar[d, twoheadrightarrow, "\tilde\pi"'] & & & T  \\
        \modulo{S}{\ideal{b_l \relatesto b'_l \given l \in L}} \ar[urrr, dashrightarrow, "\exists !"']
      \end{tikzcd}
    \end{equation*}
    
  \item \emph{Claim:} \textit{For $S \in \semz$ and a collection of elements $b_l,b'_l \in S$ for $l \in L$, we have $k(\modulo{S}{\ideal{b_l \relatesto b'_l \given l \in L}}) \isom \modulo{k(S)}{\ideal{b_l - b'_l \given l \in L}}$.
    }
    
    Let $D \in \kalg$.
    We then have via our adjunction and our universal properties the following isometries of sets, natural in $D$.
    \begin{align*}
      & \Hom_{\kalg}(k(\modulo{S}{\ideal{b_l \relatesto b'_l \given l \in L}}),D)  \\
      \isom& \Hom_{\semz}(\modulo{S}{\ideal{b_l \relatesto b'_l \given l \in L}}, D)  \\
      \isom& \set{g \in \Hom_{\semz}(S,D) \given \forall l \in L \quantcolon g(b_l) = g(b'_l)}  \\
      \isom& \set{f \in \Hom_{\kalg}(k(S),D) \given \forall l \in L \quantcolon f(b_l) = f(b'_l)}  \\
      \isom& \set{f \in \Hom_{\kalg}(k(S),D) \given \forall l \in L \quantcolon f(b_l - b'_l) = 0}  \\
      \isom& \Hom_{\kalg}(\modulo{k(S)}{\ideal{b_l - b'_l \given l \in L}},D)
    \end{align*}
    By the Yoneda Lemma, we conclude the claim.
    
  \item Let $D \in \kalg$, and let $S \subseteq D$ be a subset that is a semigroup with zero element $0$ with the multiplicative structure of $D$.
    We then notice that $S \setminus \set 0$ is a multiplicative basis of $D$ if and only if $k(S) \to D$ (obtained from $S \to D$ via our adjunction) is an isomorphism.
    
  \item For our given multiplicative basis $\B \subseteq A$, we notice that $\B_0 = \B \union \set 0$ is a semigroup with zero element $0$.
    Furthermore, $\modline{\B_0} \subseteq \modulo{A}{I}$ is a semigroup with zero element $\modline 0$ that generates $\modulo{A}{I}$.
    
    \item As $I$ is a compatible ideal, there is a collection of elements $a_m, a'_m \in \B_0$ for $m \in M$ such that $I = \ideal{a_m - a'_m \given m \in M}$.
    We then have an isomorphism of semigroups with zero element $\modulo{\B_0}{\ideal{a_m \relatesto a'_m \given m \in M}} \isom \modline{\B_0}$.
  \item We have $A \cong k(\B_0)$.
  \item We conclude
    \begin{align*}
      \modulo{A}{I}
      &\isom \modulo{k(\B_0)}{I}  \\
      &\isom \modulo{k(\B_0)}{\ideal{a_m - a'_m \given m \in M}}  \\
      &\isom k(\modulo{\B_0}{\ideal{a_m \relatesto a'_m \given m \in M}})  \\
      &\isom k(\modline{\B_0}) \mathperiod
    \end{align*}
    Therefore $\modline{\B_0}$ is a multiplicative basis of $\modulo A I$.
  \end{itemize}










  
  
  % For $\B = \B \union \set 0$, we see that $\modline{\B_0}$ is a semigroup with zero element $\modline 0$.
  % What we now want to show is that $\modulo{A}{I} \isom k \modline{\B_0}$.
  
  % Let $\set{b_i - b'_i \given i}$, where $b_i,b'_i \in \B_0$ for all $i$, be a set of generators for our compatible ideal, so $I = \ideal{\set{b_i - b'_i \given i}}$.
  % % Let $S \defeq \modline{\B_0}$ be the set of residue classes of $\B_0$ in $\modulo{A}{I}$, which is a semigroup with zero element $\modline 0$.
  % % In categorical terms, $\modline{\B_0}$ is a quotient of $\B_0$, one with the following universal property.
  
  % Then consider $\pi \colon \B_0 \twoheadrightarrow \modulo{\B_0}{\set{b_i \sim b'_i \given i}}$, the quotient of $\B_0$ characterized by the following universal property.
  % \textit{Let $\Phi \colon \B_0 \to S$ be a morphism of semigroups with zero element, meaning $\Phi(0) = \modline 0$, such that $\Phi(b_i) = \Phi(b'_i)$ for all $i$.
  % Then $\Phi$ uniquely factorizes through $\pi$, the quotient of $\B_0$ with the relations $b_i \relatesto b'_i$ for all $i$, which is the universal object with $\pi(b_i) = \pi(b'_i)$ for all $i$.}
  % \begin{equation*}
  %   \begin{tikzcd}
  %     \B_0 \ar[rrr, "\Phi(b_i) = \Phi(b'_i) \text{ for all } i"] \ar[d, twoheadrightarrow, "\pi"'] & & & S  \\
  %     \modulo{\B_0}{\set{b_i \sim b'_i \given i}}  \ar[urrr, dashrightarrow, "\exists !"']
  %   \end{tikzcd}
  % \end{equation*}
  % (We don't show that this quotient actually exists.)
  
  % Under our functor from semigroups with zero element to associative $k$-algebras, commuting diagrams not only transfer over, but we notice that $k\modline{\B_0}$ actually also the universal object, so the uniqueness of the factorization also transfers over, and we have the following universal property.
  % Let $\tilde \Phi \colon k \B_0 \to C$ be a morphism of associative $k$-algebras with $\tilde \Phi(b_i) = \tilde \Phi(b'_i)$ for all $i$.
  % Then $\tilde \Phi$ uniquely factors through $\tilde \pi \colon k\B_0 \to k(\modulo{\B_0}{\set{b_i \sim b'_i \given i}})$, the quotient with relations $b_i \relatesto b'_i$ for all $i$, which is the universal object with $\tilde \pi(b_i) = \tilde \pi(b'_i)$ for all $i$.
  % \begin{equation*}
  %   \begin{tikzcd}
  %     k\B_0 \ar[rrr, "\tilde \Phi(b_i) = \tilde \Phi(b'_i) \text{ for all } i"] \ar[d, twoheadrightarrow, "\tilde \pi"'] & & & C  \\
  %     k(\modulo{\B_0}{\set{b_i \sim b'_i \given i}}) \ar[urrr, dashrightarrow, "\exists !"']
  %   \end{tikzcd}
  % \end{equation*}
  % This means that $k(\modulo{\B_0}{\set{b_i \sim b'_i \given i}}) \isom \modulo{(k\B_0)}{\set{b_i \sim b'_i \given i}}$.
  % But we also notice that $\tilde \pi' \colon k\B_0 \to \modulo{k\B_0}{\ideal{\set{b_i - b'_i \given i}}}$ is also the universal object with $\tilde \pi'(b_i) = \tilde \pi'(b'_i)$ for all $i$.
  % Therefore $\modulo A I \isom \modulo{(k \B_0)}{\set{b_i \sim b'_i \given i}} \cong k(\modulo{\B_0}{\set{b_i \sim b'_i \given i}}) \cong k\modline{\B_0}$, which concludes the proof.
\end{proof}

\section{Path algebras}

We here present a definition of path algebras different from the one in \Cref{pathalgebra}.
It is here more quickly apparent what the multiplicative basis looks like, and how the grading arises.
(See also \cite[Definition~2.1]{dwz08})

\begin{definition}\label{ktothem}
  Let $M$ be a nonempty set.
  We denote by $k^{(M)}$ the set of all functions $f \colon M \to k$ with finite support, meaning $\numberof \set{m \in M \given f(m) \neq 0} \in \N_0$.
  We equip this with a commutative associative $k$-algebra structure by defining the additive structure by pointwise addition and the multiplicative structure by pointwise multiplication.
  The constant zero map is then the additive neutral element.
\end{definition}

\begin{lemma}\label{unitktothem}
  For a nonempty set $M$, the commutative associative $k$-algebra $k^{(M)}$ is unital if and only if $M$ is finite.
\end{lemma}

\begin{proof}
  Let $M$ be finite.
  Then $k^{(M)} = k^M$ because all maps have finite support for finite $M$.
  In particular, we have $\one_M \in k^{(M)}$, the map with constant value $1$, which then is the desired unit.

  For the converse case, we see that the unit must have value $1$ everywhere on $M$, and for this map to be in $k^{(M)}$, the set $M$ must be finite.
\end{proof}

 \begin{definition}[quiver, path algebra]\label{altpathalgebra}
  A (finite) \emph{quiver} $Q = \tuple{Q_0,Q_1,s,t}$ consists of the following data.
  \begin{itemize}
  \item A nonempty set $Q_0$, the \emph{vertices},
  \item a nonempty set $Q_1$, the \emph{arrows},
  \item a map $s \colon Q_1 \to Q_0$, the \emph{source}, and
  \item a map $t \colon Q_1 \to Q_0$, the \emph{target}.
  \end{itemize}
  
  For this data, we shall furthermore define the commutative $k$-algebras $R= k^{(Q_0)}$ and $A = k^{(Q_1)}$ as defined in \Cref{ktothem}.
  Furthermore $A$ is an $R$-bimodule via the action of the pullbacks $r.a.r' = t^\ast(r) \mult a \mult s^\ast(r') = (r \compo r) \mult a \mult (r' \compo s)$ for $r,r' \in R$ and $a \in A$.
  We now define the \emph{path algebra} of $Q = \tuple{Q_0, Q_1, s , t}$ as the graded tensor algebra of $A$ over $R$, so
  \begin{align*}
    k Q \defeq & T_R(A) \mathcomma \text{ with homogeneous decomposition } kQ = \dsum_{d \geq 0} (k Q)_d \text{ , where}  \\
    (k Q)_d = & T_R(A)_d = A^{\tensor_R d} \mathperiod
  \end{align*}
  
  This is also a graded algebra over $k$ with the same grading, with the important detail that the grade $0$ component is in general not equal to $k$.
  As the multiplication is the tensor product, we write \enquote{$\mult$} or simply nothing instead of \enquote{$\tensor_R$} for the multiplication.
  This algebra is generated as an $R$-algebra by the elements of degree $1$, and as a $k$-algebra by the elements of degree $0$ and $1$.
  As is usual with tensor algebras, for a $k$-basis $B_1$ of $A = (kQ)_1$, the algebra is generated as an $R$-algebra by $B_1$, and if $B_0$ is a $k$-basis for $R = (kQ)_0$, then $k Q$ is generated as a $k$-algebra by $B_0 \union B_1$.
  The canonical choice of bases here is $B_0 = \set{\one_v \given v \in Q_0} \subseteq R$ and $B_1 = \set{\one_a \given a \in Q_1} \subseteq A$, and we identify $Q_0 = B_0$ and $Q_1 = B_1$.

  We denote by $Q_{\geq 0}$ the set of all nonzero products of elements in $B_0$ and $B_1$, and call such elements \emph{paths}.
  We furthermore define $Q_d \defeq Q_{\geq 0} \intersect (k Q)_d$, where $d$ is called the \emph{length} of a path $v \in Q_d$.\footnote{
    Analogously we define $Q_{\geq d} = Q_{\geq 0} \intersect (k Q)_{\geq d}$.}
  For $d=0$ and $d=1$, this is consistent with our identification $Q_0 = B_0$ and $Q_1 = B_1$.
  This means that we write $\one_v = v$ for $v \in Q_0$ and $\one_a = a$ for $a \in Q_1$.
  A path of length $0$, say the path $\one_v = v$ where $v \in Q_0$, is called the \emph{empty} path at $v$.
\end{definition}

\begin{remark}
  \begin{itemize}
  \item $kQ$ is a unital algebra if and only if $Q_0$ is finite, since then $R = k^{(Q_0)} = k^{Q_0}$ is unital with unit $1_R = \one_{Q_0} \in R$ by \Cref{unitktothem}, and we then have $1_{kQ} = 1_R \in R \subseteq kQ$.
  \item We have $\spann_k Q_d = (kQ)_d$.
  \item
    Let $v,w \in Q_0$ and $a,b \in Q_1$.
    Note that we have $ab \neq 0$ if and only if $t(a) = s(b)$ and also note that $va = a$ if and only if $s(a) = v$ and $av = a$ if and only if $t(a) = v$, resulting in zero otherwise, and finally observe that $vw = \delta_{v,w}$.
    We arrive at the fact that we can uniquely write any element $x \in Q_{\geq 0}$ as
    \begin{equation*}
      x = v_0 a_1 v_1 a_2 v_2 \dotso v_{d-1} a_d v_d  \mathcomma
    \end{equation*}
    where $v_i \in Q_0$ and $a_i \in Q_1$ such that $s(a_i) = v_{i-1}$ and $t(a_i) = v_i$, where indeed $d$ is the length of $x$.
    This is in line with a different definition of the path algebra, where paths are defined as words of such form and where the multiplication is defined by specific concatenation rules.
    % For our purposes, we notice that for any $x \in Q_{\geq 0}$ we can uniquely decompose it into $x = a_1 a_2 \dotso a_d v_{d+1}$ with $a_i \in Q_1$ and $v_{d+1} \in Q_0$.
    
  \item
    We can extend the source and target functions $s$ and $t$ to all of $Q_{\geq 0}$, in fact to nonzero scalar multiples of such elements, by defining $s(y) = v_0$ and $t(y) = v_d$ for $y = \lambda \mult x$ with $\lambda \in \units k$ and $x = v_0 a_1 v_1 a_2 v_2 \dotso v_{d-1} a_r v_d \in Q_{\geq 0}$ presented as above.
    We call $s(y)$ the \emph{source} of $y$ and $t(y)$ the \emph{target} of $y$.
    With this, we have for $y$ and $z$ of this form that $y \mult z \neq 0$ if and only if $t(y) = s(z)$.
    
  \item
    It is also common to write $\one_v = 1_v$ for $v \in Q_0$, which is also used in context of the fact that these are idempotents.
    For $v,w \in Q_0$ we have that $k Q_{v,w} \defeq 1_v \mult kQ \mult 1_w = \spann_k(Q_{v,w})$, where $Q_{v,w}$ is the set of all paths that have source $v$ and target $w$.
    For $u,v,w,x \in Q_0$ we notice $\spann_k(kQ_{u,v} \mult kQ_{w,x}) = \delta_{v,w} \mult kQ_{u,x}$, and also see that $kQ = \dsum_{v,w \in Q_0} kQ_{v,w}$.
  \end{itemize}
\end{remark}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "masterarbeit_lorke_stroppel"
%%% End:
